﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WebviewController : MonoBehaviour
{
	[SerializeField] private Text m_TextStatus;
	[SerializeField] private string m_Url;
	[SerializeField] private RawImage m_RawImage;
	[SerializeField] private Button m_Button;
	[SerializeField] private RectTransform m_RectDisplay;

	private WebViewObject _WebViewObject;
	private bool _ImageShowed;

	IEnumerator Start()
	{
		_WebViewObject = (new GameObject("WebViewObject")).AddComponent<WebViewObject>();
		_WebViewObject.transform.SetParent(transform);
		_WebViewObject.Init(
			cb: (msg) =>
			{
				Debug.Log(string.Format("CallFromJS[{0}]", msg));
				m_TextStatus.text = msg;
				if (msg.Contains("data:image/jpeg;base64,"))
				{
					msg = msg.Replace("data:image/jpeg;base64,", String.Empty);
					msg = msg.Replace(' ', '+');
				}
				m_RawImage.texture = ConvertBase64ToTexture(msg);
			},
			err: (msg) =>
			{
				Debug.LogError(string.Format("CallOnError[{0}]", msg));
			},
			ld: (msg) =>
			{
				Debug.Log(string.Format("CallOnLoaded[{0}]", msg));
#if !UNITY_ANDROID
				// NOTE: depending on the situation, you might prefer
				// the 'iframe' approach.
				// cf. https://github.com/gree/unity-webview/issues/189
				_WebViewObject.EvaluateJS(@"
				  if (window && window.webkit && window.webkit.messageHandlers && window.webkit.messageHandlers.unityControl) {
					window.Unity = {
					  call: function(msg) {
						window.webkit.messageHandlers.unityControl.postMessage(msg);
					  }
					}
				  } else {
					window.Unity = {
					  call: function(msg) {
						window.location = 'unity:' + msg;
					  }
					}
				  }
				");
#endif
				// webViewObject.EvaluateJS(@"Unity.call('ua=' + navigator.userAgent)");
			},
			// ua: "custom user agent string",
			enableWKWebView: true);
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
		_WebViewObject.bitmapRefreshCycle = 1;
#endif

		//use only size to display web view 
		//it isn't display on image texture.
		Vector2 size = Vector2.Scale(m_RectDisplay.rect.size, m_RectDisplay.lossyScale);
		var rect = new Rect(m_RectDisplay.position, size);
		var leftMargin = rect.x;
		var rightMargin = Screen.width - rect.x - rect.width;
		var topMargin = Screen.height - rect.y - rect.height;
		var bottomMargin = rect.y;

		_WebViewObject.SetMargins((int)leftMargin, (int)topMargin, (int)rightMargin, (int)bottomMargin);
		_WebViewObject.SetVisibility(true);

		_WebViewObject.LoadURL(m_Url);
		yield break;
	}

	private Texture2D ConvertBase64ToTexture(string _base64)
	{
		Debug.Log($"Base64 = {_base64}");
		var decodeToBytes = System.Convert.FromBase64String(_base64);
		var tex = new Texture2D(1, 1);
		tex.LoadImage(decodeToBytes);
		tex.Apply();
		return tex;
	}

	public void ToggleView(){
		_ImageShowed = !_ImageShowed;
		m_RawImage.gameObject.SetActive(_ImageShowed);
		m_RectDisplay.gameObject.SetActive(!_ImageShowed);
		_WebViewObject.SetVisibility(!_ImageShowed);
	}
}
